/*IMPORT FROM PACKAGES*/
import {useState} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

/**********************************************/

export default function CourseCard({courseProp}){
	// console.log(courseProp);
	// results: php-laravel {courseData[0]}
	// console.log(typeof courseProp);
	// results: object

	// object destructuring
	const {name, description, price} = courseProp;

	/*REMOVE AS ITS NOT NEEDED*/
	// react hooks - useState -> store its state
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue);
	/*const [count, setCount] = useState(0);
	console.log(useState(0));
	const [seatCount, setSeatCount] = useState(10);
	//console.log(useState(10));

	function enroll(){

		if(seatCount === 0){
			alert("No more seats available, check back later");
		} else{
			setCount(count + 1);
			setSeatCount(seatCount - 1);
			console.log(`Enrollees: ${count}`);
			console.log(`Seats: ${seatCount}`);
		}
	};*/

	return(
		<Row className="mt-3 mb-3">
			<Col xs={12}>
				<Card className="cardCourseCard p-3 ">
					<Card.Body>
						<Card.Title>
							<h4>{name}</h4>
						</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>
							{description}
						</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Link className="btn btn-primary" to="/courseView">View Details</Link>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
};