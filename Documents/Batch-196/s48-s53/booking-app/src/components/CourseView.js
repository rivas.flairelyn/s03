
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState} from 'react';

/**********************************************/

export default function CourseView(){

	const [name, setName] = useState("");
	const [description, setdescription] = useState("");
	const [price, setprice] = useState(0);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8:00 AM to 5:00 PM</Card.Text>
							<Button variant="primary">Enroll</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>


	)
}