import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){

	// Allow us to consume the User context object and it's properties to use for validation
	const {user, setUser} = useContext(UserContext);
	console.log(user);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function loginUser(e){

		e.preventDefault();

		// Syntax:
			// fetch('url', options)
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken);
				retieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Booking App of 196!"
				});
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your credentials"
				});
			};
		});

		// Set the email of the authenticated user in the local storage
			// Syntax:
				// localStorage.setItem('propertyName/key', value);
		// localStorage.setItem("email", email);

		// Access user information through localStorage to updaye the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout

		// When states change components are rerendered and the AppNavBar component will be updated based on the credentials
		// setUser({
		// 	email: localStorage.getItem('email')
		// });

		// clear input fields
		setEmail('');
		setPassword('');
	};

	const retieveUserDetails = (token) => {

		fetch('http://localhost:4000/users/getUserDetails',{
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		});
	};

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		}else{
			setIsActive(false);
		}

	}, [email, password]);

	return(
		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>
		<h1>Login Here</h1>
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group className="mt-3" controlId="loginEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="loginPassword">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>
			
			{ isActive ?
				<Button className="mt-3 mb-3" variant="success" type="submit" id="loginBtn">
					Login
				</Button>

				:

				<Button className="mt-3 mb-3" variant="danger" type="submit" id="loginBtn" disabled>
					Login
				</Button>
			}
		</Form>
		</>
	)
}