/*IMPORT FROM PACKAGES*/
import {useState, useEffect} from 'react';

/*IMPORT FROM SRC*/
import CourseCard from '../components/CourseCard';
import coursesData from '../data/coursesData';


export default function Courses(){
	// this is to check and see if the mock data is captured
	// console.log(coursesData);
	// console.log(coursesData[0]);

	/*UPDATED [COURSE-CARD]*/
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch("http://localhost:4000/courses")
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				return(
				<CourseCard key={course._id} courseProp = {course}/>
				)
			}))
		})
	}, []);

	/*COMMENT OUT AS UPGRADED THE CODE ABOVE [COURSE-CARD]*/
	// const courses = coursesData.map(course => {
	// 	console.log(course);
	// 	return(
	// 		<CourseCard key={course.id} courseProp = {course}/>
	// 	);
	// });

	return(

		<>
			<h1>Available Courses:</h1>
			{courses}
		</>

	)
}