// Import Mongoose
const mongoose = require("mongoose");

// Create order schema
const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required: [true, "Please input dummy Total Amount"]
	}, 
	purchasedOn: {
		type: Date,
		defaults: new Date()
	},
	userId: {
		type: String,
		required: [true, "Please input your User Id"]
	},
	products: [
		{
			
			productId: {
				type: String,
				required: [true, "Please input the product id you want to purchase"]
			},
			quantity: {
				type: Number,
				required: [true, "Enter the quantity"]
			}
		
		}

	]

})


// Exports User schema
module.exports = mongoose.model("Order",orderSchema);