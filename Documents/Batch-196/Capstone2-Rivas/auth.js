// Import JWT
const jwt = require("jsonwebtoken");
const secret = "ecommerceAPI"

module.exports.createAccessToken = (userDetails) => {

	// console.log(userDetails);

	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}

	// console.log(data);

	return jwt.sign(data,secret,{});

}

module.exports.verify = (req,res,next) => {

	let token = req.headers.authorization

	if (typeof token === "undefined"){
		return res.send({auth: "Failed. No Token."});
	} else {

		token = token.slice(7);

		jwt.verify(token,secret,function(error,decodedToken) {

			if (error){
				return res.send({
					auth: "Failed",
					message: error.message
				})
			} else {
				req.user = decodedToken;
				next();
			}
		})
	}

}

// verifyAdmin as a middleware.

module.exports.adminVerification = (req,res,next) => {

	// console.log(req.user);
	if (req.user.isAdmin){
		next();
	} else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}