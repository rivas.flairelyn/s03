// Import Express
const express = require("express");
// Import Mongoose
const mongoose = require("mongoose");

// Called express
const app = express();
const port = process.env.PORT || 4000;
const cors = require('cors');


mongoose.connect("mongodb+srv://admin:admin123@cluster0.mzv6irg.mongodb.net/ecommerceAPI?retryWrites=true&w=majority",
{

	useNewUrlParser: true,
	useUnifiedTopology: true

});

// Identify if you successfully connection or not
let db = mongoose.connection;
db.on('error', console.error.bind(console, "MongoDB Connection Error."));
db.once('open',()=>console.log("Successfully connected to MongoDB"));

app.use(express.json());
// middleware (cors-cross origin resource sharing)
app.use(cors());

// import the user routes
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

// import the product routes
const productRoutes = require('./routes/productRoutes');
app.use('/products', productRoutes);

// import the order routes
const orderRoutes = require('./routes/orderRoutes');
app.use('/orders', orderRoutes);

app.listen(port,()=>console.log(`Server is running at port ${port}`));
