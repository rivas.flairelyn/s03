// Import user model
const User = require("../models/User");
// Import bcrypt
const bcrypt = require("bcrypt");
// import auth
const auth = require("../auth");

// User Registration
module.exports.registerUser = (req,res) =>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);


	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo
	});

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error));

};

// User Login
module.exports.loginUser = (req,res) => {

	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send({message: "No User Found."})
		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			if (isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send({message: "Incorrect Password"});
			}
		}
	})
}

// Update User by Admin Only
module.exports.updateUser = (req,res) => {


	let update = {

		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}
// Get Own User Details
module.exports.getUserDetails = (req,res) => {

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

// Check email if existing
module.exports.checkEmailExists = (req,res) => {

	User.findOne({email: req.body.email})
	.then(result => {

		if(result === null){
			return res.send(false);
		} else {
			return res.send(true)
		}

	})
	.catch(err => res.send(err));
}