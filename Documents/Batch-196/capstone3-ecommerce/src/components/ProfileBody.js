
import { useAccordionButton,Accordion,Card } from 'react-bootstrap';
import {useEffect} from 'react'

/**********************************************/
export default function ProfileBody({userProp}){

  const{firstName, lastName, mobileNo, isAdmin, _id} = userProp;

  function CustomToggle({ children, eventKey }) {
    const decoratedOnClick = useAccordionButton(eventKey);

    return (
      <button className="profileBodyBtn" type="button" onClick={decoratedOnClick}>
        {children}
      </button>
    );
  }

  const admin = isAdmin.toString();

  
  return (  
  <>
    {(isAdmin===true)?
    <Accordion defaultActiveKey="">
      <Card className="profileBodyCard">
        <Card.Header>
          <CustomToggle eventKey="0">My Account</CustomToggle>
        </Card.Header>
        <Accordion.Collapse eventKey="0">
          <Card.Body>
            <Card.Title>Account Name:</Card.Title>
            <Card.Text>
              {firstName} {lastName}
            </Card.Text>
            <Card.Title>Mobile Number:</Card.Title>
            <Card.Text>
              {mobileNo}
            </Card.Text>
            <Card.Title>Member since:</Card.Title>
            <Card.Text>
              Comming Soon
            </Card.Text>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
    </Accordion>

    :

    <Accordion defaultActiveKey="">
      <Card className="profileBodyCard">
        <Card.Header>
          <CustomToggle eventKey="0">My Account</CustomToggle>
        </Card.Header>
        <Accordion.Collapse eventKey="0">
          <Card.Body>
            <Card.Title>Account Name:</Card.Title>
            <Card.Text>
              {firstName} {lastName}
            </Card.Text>
            <Card.Title>Mobile Number:</Card.Title>
            <Card.Text>
              {mobileNo}
            </Card.Text>
            <Card.Title>Admin:</Card.Title>
            {(admin !== true)?
            <Card.Text>
              Yes
            </Card.Text>
            :
            <Card.Text>
              No
            </Card.Text>
            }
            <Card.Title>Member since:</Card.Title>
            <Card.Text>
              Comming Soon
            </Card.Text>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card className="profileBodyCard">
        <Card.Header>
          <CustomToggle eventKey="1">My Order History</CustomToggle>
        </Card.Header>
        <Accordion.Collapse eventKey="1">
          <Card.Body>
            <Card.Title>Order Created:</Card.Title>
            <Card.Text>
              Comming Soon
            </Card.Text>
            <Card.Title>Product Name:</Card.Title>
            <Card.Text>
              Comming Soon
            </Card.Text>
            <Card.Title>Quantity:</Card.Title>
            <Card.Text>
              Comming Soon
            </Card.Text>
            <Card.Title>Price:</Card.Title>
            <Card.Text>
              Comming Soon
            </Card.Text>
            <Card.Title>Total:</Card.Title>
            <Card.Text>
              Comming Soon
            </Card.Text>
          </Card.Body>
        </Accordion.Collapse>
      </Card>

      <Card className="profileBodyCard">
        <Card.Header>
          <CustomToggle eventKey="2">My Cart Bag</CustomToggle>
        </Card.Header>
        <Accordion.Collapse eventKey="2">
          <Card.Body>
            <Card.Title>Product Name:</Card.Title>
            <Card.Text>
              Comming Soon
            </Card.Text>
            <Card.Title>Quantity:</Card.Title>
            <Card.Text>
              Comming Soon
            </Card.Text>
            <Card.Title>Price:</Card.Title>
            <Card.Text>
              Comming Soon
            </Card.Text>
            <Card.Title>Total:</Card.Title>
            <Card.Text>
              Comming Soon
            </Card.Text>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
    </Accordion>
    }
  </>
  
  );
}
